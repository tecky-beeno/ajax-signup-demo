import express from 'express';
import bodyParser from 'body-parser';
let app = express();
let port = 8100;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.post('/signup', (req, res) => {
  try {
    console.log(req.body);
    if (req.body.password == 'secret') {
      res.json({ ok: true });
    } else {
      res.json({ ok: false, errors: ['wrong password', 'wrong username'] });
    }
  } catch (err) {
    res.json({ ok: false, errors: [err.toString()] });
  }
});

app.use(express.static('public'));

app.listen(port, () => {
  console.log('listening on http://localhost:' + port);
});
